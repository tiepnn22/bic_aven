<footer>
	<div class="container">
		<div class="bao">
			<div class="main-footer">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-left">
						<div class="info-footer">
							<span class="text">
								<h4><?php echo get_theme_mod( 'title_left' ); ?></h4>
							</span>
							<div class="footer-logo">
								<a href="<?php echo get_option('home');?>">
									<img src="<?php echo get_theme_mod( 'logo_footer' ); ?>">
								</a>
							</div>
							<address>
		                        <?php echo get_theme_mod( 'info' );?>
		                    </address>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-right">
						<span class="text">
							<h4><?php echo get_theme_mod( 'title_right' ); ?></h4>
						</span>
						<div class="group">
							<address>
								<?php echo get_theme_mod( 'address_left' ); ?>
							</address>
							<address>
								<?php echo get_theme_mod( 'address_right' ); ?>
							</address>
						</div>
					</div>
				</div>
			</div>
			<div class="footer">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 copyright">
						<?php echo get_theme_mod( 'copyright' ); ?>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 design">
						<?php echo get_theme_mod( 'designby' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<div id="back-to-top">
    <a href="javascript:void(0)">
    	<span class="fa fa-arrow-up"></span>
    </a>
</div>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/596b00b31dc79b329518e861/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->




<?php wp_footer(); ?>
</body>
</html>