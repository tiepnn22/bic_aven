<?php
	global $post;
	$terms = get_the_terms( $post->ID , 'diem-ban-category', 'string');
	$term_ids = wp_list_pluck($terms,'term_id');
	$query = new WP_Query( array(
		'post_type' => 'diem-ban',
		'tax_query' => array(
			array(
				'taxonomy' => 'diem-ban-category',
				'field' => 'id',
				'terms' => $term_ids,
				'operator'=> 'IN'
			 )),
		'posts_per_page' => 4,
		'orderby' => 'date',
		'post__not_in'=>array($post->ID)
	 ) );
?>
<aside class="related-post fw">
	<div class="caption-text">
		<span class="text">
			<h3>Điểm bán liên quan</h3>
		</span>
	</div>
	<?php
	if($query->have_posts()) {
		echo '<ul>';
		while($query->have_posts()) { $query->the_post(); ?>
			<li class="title">
		      <a href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>">
		      	<h4><?php the_title(); ?></h4>
		      </a>
		    </li>
			<?php
		}
		echo '</ul>';
	 wp_reset_query(); } ?>
</aside>