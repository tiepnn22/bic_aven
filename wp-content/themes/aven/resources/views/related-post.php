<aside class="related-post fw">
	<div class="caption-text">
		<span class="text">
			<h3>Tin liên quan</h3>
		</span>
	</div>
	<?php
	    global $post;
		$orig_post = $post;
	    $categories = get_the_category($post->ID);
	    if ($categories) {
		    $category_ids = array();
		    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
		    $args=array(
			    'category__in' => $category_ids,
			    'post__not_in' => array($post->ID),
			    'posts_per_page'=> 4,
			    'ignore_sticky_posts'=>1
		    );
		    $my_query = new wp_query( $args );
		    if( $my_query->have_posts() ) {
			    echo '<ul>';
			    while( $my_query->have_posts() ) {
				    $my_query->the_post();?>
					    <li class="title">
					      <a href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>">
					      	<h4><?php the_title(); ?></h4>
					      </a>
					    </li>
				    <?php
			    }
			    echo '</ul>';
		    }
	    }
	    $post = $orig_post;
	    wp_reset_query();
	?>
</aside>