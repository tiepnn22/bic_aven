<section class="page-content">
	<div class="container">
		<div class="row">
			<div class="bao">
				<main class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page-article">
					<div class="article-details">
						<div class="article-content">
							<?php while (have_posts()) : the_post(); ?>
								<article class="detail">
									<div class="title-detail">
										<h1><?php the_title();?></h1>
									</div>
									<div class="excerpt">
										<b style="float: left;">Câu hỏi : &nbsp;</b> <?php the_excerpt();?>
									</div>
									<div class="name-faq">
										<b>Người gửi : &nbsp;</b>
										<?php echo types_render_field("ask-name", array("output"=>"normal")); ?>
										(<i><?php echo types_render_field("ask-email", array("output"=>"normal")); ?></i>)
									</div>
									<div class="description">
										<b style="float: left;">Trả lời : &nbsp;</b> <?php the_content();?>
									</div>
									<div class="share">
										<?php get_template_part("resources/views/social-bar");?>
									</div>
								</article>
							<?php endwhile; wp_reset_query(); ?>
						</div>
						<?php get_template_part('resources/views/related-hoi-dap'); ?>
					</div>
					<?php get_template_part("resources/views/ads"); ?>
				</main>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>