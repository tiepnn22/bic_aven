<section class="page-content">
	<div class="container">
		<div class="row">
			<div class="bao">
				<main class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page-article">
					<div class="article-details">
						<div class="article-content">
							<?php while (have_posts()) : the_post(); ?>
								<article class="detail">
									<div class="title-detail">
										<h1><?php the_title();?></h1>
									</div>
									<div class="description">
										<?php the_content();?>
									</div>
									<div class="share">
										<?php get_template_part("resources/views/social-bar");?>
									</div>
								</article>
							<?php endwhile; wp_reset_query(); ?>
						</div>

						<div class="adv-article">
							<div class="adv-article-left">
								<a href="<?php echo get_post_type_archive_link('diem-ban')?>">
									<img src="<?php bloginfo('template_url');?>/dist/images/img/suicide-left.jpg">
								</a>
							</div>
							<div class="adv-article-center">
								<a href="tel:18006626">
									<img src="<?php bloginfo('template_url');?>/dist/images/img/suicide-phone.jpg">
								</a>
							</div>
							<div class="adv-article-right">
								<a href="">
									<img src="<?php bloginfo('template_url');?>/dist/images/img/suicide-right.jpg">
								</a>
							</div>
						</div>

						<?php get_template_part('resources/views/related-post'); ?>
						<div class="box-fb-comment">
							<div class="fb-comments" data-href="" data-colorscheme="light" data-numposts="5" data-width="500"></div>
						</div>
					</div>
					<?php get_template_part("resources/views/ads"); ?>
				</main>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1606057776088685',
      xfbml      : true,
      version    : 'v2.9'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>