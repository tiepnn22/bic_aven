<?php
	global $post;
	$terms = get_the_terms( $post->ID , 'hoi-dap-category', 'string');
	$term_ids = wp_list_pluck($terms,'term_id');
	$query = new WP_Query( array(
		'post_type' => 'hoi-dap',
		'tax_query' => array(
			array(
				'taxonomy' => 'hoi-dap-category',
				'field' => 'id',
				'terms' => $term_ids,
				'operator'=> 'IN'
			 )),
		'posts_per_page' => 4,
		'orderby' => 'date',
		'post__not_in'=>array($post->ID)
	 ) );
?>
<div class="related-products fw">
	<div  class="ask-news">
		<span class="text">
			<h3>Câu hỏi mới nhất</h3>
		</span>
	</div>
	<div class="ask-content fw">
	<?php while($query->have_posts()) : $query->the_post(); ?>
		<article class="ask-item">
			<div class="title-faq"><a href="<?php the_permalink();?>"><h3><?php the_title();?></h3></a></div>
			<div class="date-faq"><b>Ngày gửi : &nbsp;</b><?php echo get_the_date(); ?></div>
			<div class="desc-faq"><b>Câu hỏi : &nbsp;</b><?php the_excerpt();?></div>
			<a class="btn-faq" href="<?php the_permalink();?>"> >> Xem câu trả lời</a>
		</article>
	<?php endwhile;  wp_reset_query(); ?>
	</div>
</div>