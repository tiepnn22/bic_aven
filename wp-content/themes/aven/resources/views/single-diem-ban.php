<section class="page-content">
	<div class="container">
		<div class="row">
			<div class="bao">
				<main class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page-article">
					<div class="article-details">
						<div class="article-content">
							<?php while (have_posts()) : the_post(); ?>
								<article class="detail">
									<div class="title-detail">
										<h1><?php the_title();?></h1>
									</div>
									<div class="description">
										<?php the_content();?>
									</div>
									<div class="share">
										<?php get_template_part("resources/views/social-bar");?>
									</div>
								</article>
							<?php endwhile; wp_reset_query(); ?>
						</div>
						<?php get_template_part('resources/views/related-diem-ban'); ?>
					</div>
					<?php get_template_part("resources/views/ads"); ?>
				</main>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>