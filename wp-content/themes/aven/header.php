<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title><?php echo title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header class="header">
	<div class="container">
		<div class="bao header-content">
			<div class="logo">
				<a href="<?php echo get_option('home');?>">
					<img src="<?php echo get_theme_mod( 'logo' );?>">
				</a>
			</div>
			<div class="header-right">
				<div class="sitename">
					<span class="text-action">
						<?php echo get_theme_mod( 'slogan_small' );?>
					</span>
					<span class="text-name">
						<?php echo get_theme_mod( 'slogan_large' );?>
					</span>
				</div>
				<div class="hotline">
					<span class="text">
						<?php echo get_theme_mod( 'phone_text' );?>
					</span>
					<span class="number">
						<a href="tel:<?php echo get_theme_mod( 'phone_tel' );?>">
							<?php echo get_theme_mod( 'phone' );?>
						</a>
					</span>
					<div class="bg-phone">
						<i class="fa fa-phone" aria-hidden="true"></i>
					</div>
				</div>
			</div>
		</div>
		<nav class="menu">
			<div class="main-menu">
				<?php
	                if(function_exists('wp_nav_menu')){
	                    $args = array(
	                        'theme_location' => 'primary',
	                        'link_before'=>'',
	                        'link_after'=>'',
	                        'container_class'=>'',
	                        'menu_class'=>'menu-primary',
	                        'menu_id'=>'',
	                        'container'=>'ul',
	                        'before'=>'',
	                        'after'=>''
	                    );
	                    wp_nav_menu( $args );
	                }
	            ?>
			</div>
			<div class="mobile-menu"></div>
			<ul class="menu-mobile-link">
				<li><a href="<?php echo get_option('home');?>">Trang chủ</a></li>
				<li>
    				<a href="<?php echo get_theme_mod( 'category_mobile_link' ); ?>">
    					<h2><?php echo get_theme_mod( 'category_mobile' );?></h2>
    				</a>
				</li>
			</ul>
		</nav>
	</div>
</header>
