<?php
	/*
	Template Name: Trang hỏi đáp
	*/
?>

<?php get_header(); ?>

<?php
	$terms = get_terms( 'hoi-dap-category', array(
		'parent'=> 0,
	    'hide_empty' => false
	) );
	$taxonomy_name = 'hoi-dap-category';
?>

<section class="page-content">
	<div class="container">
		<div class="row">
			<div class="bao">
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page-article">
					<div class="article-list n-items">

						<?php
							foreach($terms as $term){
						?>
							<span class="map">
								<h2><?php echo $term->name; ?></h2>
							</span>

							<?php get_template_part("resources/views/faq"); ?>

							<span class="ask-news">
								<span><h3>Câu hỏi mới nhất</h3></span>
							</span>
							<div class="ask-content fw">
								<?php
								$query = aven_custom_posttype_query('hoi-dap', 'hoi-dap-category', $term->term_id, 1000);
								while ($query->have_posts() ) : $query->the_post(); ?>
									<article class="ask-item">
										<div class="title-faq">
											<a href="<?php the_permalink();?>"><h3><?php the_title();?></h3></a>
										</div>
										<div class="name-faq">
											<b>Người gửi : &nbsp;</b>
											<?php echo types_render_field("ask-name", array("output"=>"normal")); ?>
											(<i><?php echo types_render_field("ask-email", array("output"=>"normal")); ?></i>)
										</div>
										<div class="date-faq">
											<b>Ngày gửi : &nbsp;</b><?php echo get_the_date(); ?>
										</div>
										<div class="desc-faq">
											<b>Câu hỏi : &nbsp;</b> <?php the_excerpt();?>
										</div>
										<a class="btn-faq" href="<?php the_permalink();?>"> >> Xem câu trả lời</a>
									</article>
								<?php endwhile; wp_reset_postdata(); ?>
							</div>
						<?php } ?>
						
					</div>
					<?php get_template_part("resources/views/ads"); ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>