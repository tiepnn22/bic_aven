<?php get_header(); ?>

<?php
	$current_category = get_category_by_slug( get_query_var( 'category_name' ) );
	$cat_id = $current_category->term_id;
	$sticky = get_option( 'sticky_posts' );
?>

<section class="page-content">
	<div class="container">
		<div class="row">
			<div class="bao">
				<div class="stick-content">
					<div class="bao-vien">
					<?php
						if(isset($sticky) && !empty($sticky)){
							echo '
								<article class="col-lg-7 col-md-7 col-sm-7 col-xs-12 item-first" id="item-first"></article>
								<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 item-no-first" id="item-no-first">';

							$query = new WP_Query(array('cat'=>$cat_id,'post__in' => $sticky,'showposts'=>4)); $t=0;
							while ($query->have_posts() ) : $query->the_post();
								echo '
									<article class="item">
										<figure>
											<a href="'.get_the_permalink().'">
												<img class="img-responsive" src="'.getPostImage(get_the_ID(),"full").'" alt="'.get_the_title().'"/>
											</a>
										</figure>
										<div class="item-content">
											<div class="title">
												<a href="'.get_the_permalink().'">
													<h2>'.get_the_title().'</h2>
												</a>
											</div>
											<div class="date">('.get_the_date().')</div>
										</div>
									</article>';
							$t++; endwhile; wp_reset_postdata();
							echo '</div>';
						}
					?>
					</div>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page-article">
					<div class="article-list n-items">
						<?php
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
							$query = new WP_Query(array('cat'=>$cat_id,'post__not_in' => $sticky,'order' => 'DESC','orderby' => 'date', 'paged'=> $paged ));
							while ($query->have_posts() ) : $query->the_post();
								echo '
									<article class="item">
										<figure>
											<a href="'.get_the_permalink().'">
												<img class="img-responsive" src="'.getPostImage(get_the_ID(),"p-thumb").'" alt="'.get_the_title().'"/>
											</a>
										</figure>
										<div class="item-content">
											<div class="title">
												<a href="'.get_the_permalink().'">
													<h2>'.get_the_title().'</h2>
												</a>
											</div>
											<div class="date">('.get_the_date().')</div>
											<div class="desc">'.cut_string(get_the_excerpt(),400,'...').'</div>
										</div>
									</article>';
							endwhile; wp_reset_postdata();
						?>
					</div>
					<nav class="navigation">
						<?php wp_pagenavi( array( 'query' => $query ) ); ?>
					</nav>
					<?php get_template_part("resources/views/ads"); ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>