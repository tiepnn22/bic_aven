<?php
	/*
	Template Name: Trang điểm bán
	*/
?>

<?php get_header(); ?>

<?php
	$terms = get_terms( 'diem-ban-category', array(
		'parent'=> 0,
	    'hide_empty' => false
	) );
	$taxonomy_name = 'diem-ban-category';
?>

<section class="page-content">
	<div class="container">
		<div class="row">
			<div class="bao">
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page-article">
					<div class="article-list n-items">

						<?php
							foreach($terms as $termss){
						?>
								<span class="map">
									<i class="fa fa-map-marker" aria-hidden="true"></i><h2><?php echo $termss->name; ?></h2>
								</span>
								<div class="row">
									<?php
									$query = aven_custom_posttype_query_order('diem-ban', 'diem-ban-category', $termss->term_id, 1000);
									while ($query->have_posts() ) : $query->the_post(); ?>
										<article class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
											<div class="title">
												<a href="<?php the_permalink();?>">
													<h3><?php the_title();?></h3>
												</a>
											</div>
										</article>
									<?php endwhile; wp_reset_postdata(); ?>
								</div>
						<?php	
							}
						?>
					
					</div>
					<?php get_template_part("resources/views/ads"); ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>