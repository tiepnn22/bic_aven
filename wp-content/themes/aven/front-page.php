<?php get_header(); ?>

<section class="banner banner-home">
	<div class="container">
		<div class="bao">
			<div class="bn-item">
				<figure>
					<img src="<?php echo get_field('home_banner'); ?>" />
				</figure>
			</div>
		</div>
	</div>
</section>

<section class="featured-product">
	<div class="container">
		<div class="row">
			<div class="bao featured-home">

				<?php
					$home_feature = get_field('home_feature');
					foreach ($home_feature as $home_feature_kq) {
						$home_feature_kq = $home_feature_kq['home_feature_cat'];
				?>

				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 featured-item">
				    <div class="category-title">
				    	<a href="<?php echo esc_url(get_term_link($home_feature_kq));?>">
				    		<h2><?php echo get_cat_name($home_feature_kq);?></h2>
				    	</a>
				    </div>
				    <div class="featured-content">
				        <?php
				            $query = new WP_Query(array('cat'=>$home_feature_kq,'showposts'=>4)); $j = 0;
				            while ($query->have_posts() ) : $query->the_post();

					            if($j == 0) { ?>

					                <article class="item first">
					                    <figure>
					                    	<a href="<?php the_permalink();?>">
					                    		<img src="<?php echo getPostImage(get_the_ID(),'full'); ?>" alt="<?php the_title(); ?>" />
					                    	</a>
					                    </figure>
					                    <div class="item-content">
					                        <div class="title">
					                        	<a href="<?php the_permalink();?>">
					                        		<h3><?php the_title();?></h3>
					                        	</a>
					                        </div>
					                        <div class="desc">
					                        	<?php echo cut_string(get_the_excerpt(),400,'...');?>
				                        	</div>
					                    </div>
					                </article>

					        	<?php } else { ?>

					                <article class="item">
					                    <div class="item-content">
					                        <div class="title">
					                        	<a href="<?php the_permalink();?>">
					                        		<h3><?php the_title();?></h3>
					                        	</a>
					                        </div>
					                    </div>
					                </article>
					        	<?php }
				            $j++; endwhile; wp_reset_postdata();
				        ?>
				    </div>
				</div>

				<?php } ?>

			</div>
		</div>
	</div>
</section>

<section class="main-home">
    <div class="container">
	    <div class="row">
		    <div class="bao main-content">
		        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page-content">

					<?php
						$home_show = get_field('home_show');
						foreach ($home_show as $home_show_kq) {
							$home_show_kq = $home_show_kq['home_show_cat'];
					?>

		        	<div class="home-product">
		        		<div class="page-title">
		        			
		        			<div class="category-title">
		        				<a href="<?php echo esc_url(get_term_link($home_show_kq));?>">
		        					<h2><?php echo get_cat_name($home_show_kq);?></h2>
		        				</a>
		        			</div>
		        			<a href="<?php echo esc_url(get_term_link($home_show_kq));?>" class="btn-readall">Xem tất cả </a>
		        		</div>
		        		<div class="home-product-content">
		        		<?php
		        			$query = new WP_Query(array('cat'=>$home_show_kq,'showposts'=>4,'order' => 'DESC','orderby' => 'date')); $i=0;
							while ($query->have_posts() ) : $query->the_post();
						?>
			        		<article class="item">
								<?php if($i == 0) { ?>
									<figure>
										<a href="<?php the_permalink();?>">
											<img src="<?php echo getPostImage(get_the_ID(),'p-thumb'); ?>" alt="<?php the_title(); ?>" />
										</a>
									</figure>
									<div class="item-content">
								<?php } ?>
										<div class="title">
											<a href="<?php the_permalink();?>">
												<h3><?php the_title();?></h3>
											</a>
										</div>
								<?php if($i == 0) { ?>
										<div class="desc">
											<?php echo cut_string(get_the_excerpt(),400,'...');?>
										</div>
									</div>
								<?php } ?>
							</article>
						<?php $i++; endwhile; wp_reset_postdata(); ?>
						</div>
		        	</div>

					<?php } ?>

					<?php
						$home_ask = get_field('home_ask');
						$home_url_ask = get_field('home_url_ask');
						$image_cat = get_field('image_cat','category_'.$home_ask.'');
					?>
					<div class="home-product">
		        		<div class="page-title">
		        			<div class="category-title">
		        				<a href="<?php echo $home_url_ask; ?>">
		        					<h2><?php echo get_term($home_ask, 'hoi-dap-category')->name;?></h2>
		        				</a>
		        			</div>
		        			<a href="<?php echo $home_url_ask; ?>" class="btn-readall">Xem tất cả </a>
		        		</div>
		        		<div class="home-product-content">
			        		<article class="item">
								<figure>
									<a href="<?php echo $home_url_ask; ?>">
										<img class="img-responsive" src="<?php echo $image_cat; ?>">
									</a>
								</figure>
								<div class="item-content">
									<div class="title">
										<span>Đặt câu hỏi cho chuyên gia</span>
									</div>
									<?php get_template_part("resources/views/faq"); ?>
								</div>
							</article>
							<?php
			        			$query = aven_custom_posttype_query('hoi-dap', 'hoi-dap-category', $home_ask, 3);
								while ($query->have_posts() ) : $query->the_post();
							?>
								<div class="title">
									<a href="<?php the_permalink();?>">
										<h3><?php the_title();?></h3>
									</a>
								</div>
							<?php endwhile; wp_reset_postdata(); ?>
						</div>
		        	</div>
					<?php get_template_part("resources/views/ads"); ?>
		        </div>
		        <?php get_sidebar(); ?>
		    </div>
	    </div>
    </div>
</section>

<?php get_footer(); ?>

