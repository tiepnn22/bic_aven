<?php
//include widget
include_once get_template_directory(). '/resources/widgets/show-post.php';
include_once get_template_directory(). '/load/customize.php';

//add style
function aven_style() {
    wp_register_style( 'main-style', get_stylesheet_directory_uri() . "/dist/css/style.all.css",false, 'all' );
    wp_enqueue_style('main-style');

	wp_register_script( 'main-script', get_stylesheet_directory_uri() . "/dist/js/main.all.js", array('jquery'),true );
	wp_enqueue_script('main-script');

    $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
    $params = array(
        'ajax_url' => admin_url('admin-ajax.php', $protocol),
    );
    wp_localize_script('template-scripts', 'ajax_obj', $params);
}
if (!is_admin()) add_action('wp_enqueue_scripts', 'aven_style');


//xoa class va id wp_nav_menu()
function wp_nav_menu_attributes_filter($var) {
	return is_array($var) ? array_intersect($var, array('current-menu-item','mega-menu')) : '';
}
add_filter('nav_menu_css_class', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('page_css_class', 'wp_nav_menu_attributes_filter', 100, 1);


//đăng ký menu và img
if (!function_exists('avenSetup')) {
    function avenSetup()
    {
        // Register menus
        register_nav_menus( array(
            'primary' => __( 'Menu chính', 'aven' ),
        ) );

        // add_theme_support('menus');
	    add_theme_support( 'post-thumbnails' );
	    add_image_size( 'p-thumb', 230, 150, true );
	    add_image_size( 'p-detail', 120, 75, true );
	    add_image_size( 'p-medium', 68, 45, true );
    }
    add_action('after_setup_theme', 'avenSetup');
}

//dang ky sidebar
if (!function_exists('avenWidgets')) {
    function avenWidgets()
    {
        $sidebars = [
            [
				'name'          => __( 'Sidebar', 'aven' ),
				'id'            => 'sidebar',
				'description'   => __( 'Sidebar', 'aven' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
            ],
            [
				'name'          => __( 'Vùng quảng cáo', 'aven' ),
				'id'            => 'adv-ads',
				'description'   => __( 'Vùng quảng cáo dưới chân trang', 'aven' ),
				'before_widget' => '<div id="%1$s" class="widget col-lg-6 col-md-6 col-sm-6 col-xs-6 %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '',
				'after_title'   => '',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }
    add_action('widgets_init', 'avenWidgets');
}

//query custom posttype
function aven_custom_posttype_query($posttype, $taxonomy, $termId, $numPost){
    $qr =  new WP_Query( array(
                        'post_type' => $posttype,
                        'tax_query' => array(
                                            array(
                                                    'taxonomy' => $taxonomy,
                                                    'field' => 'id',
                                                    'terms' => $termId,
                                                    'operator'=> 'IN'
                                             )),
                        'showposts'=>$numPost,
                        'order' => 'DESC',
                        'orderby' => 'date'
                 ) );
    return $qr;
}
function aven_custom_posttype_query_order($posttype, $taxonomy, $termId, $numPost){
    $qr =  new WP_Query( array(
                            'post_type' => $posttype,
                            'tax_query' => array(
                                                array(
                                                        'taxonomy' => $taxonomy,
                                                        'field' => 'id',
                                                        'terms' => $termId,
                                                        'operator'=> 'IN'
                                                 )),
                            'showposts'=>$numPost,
                            'order' => 'ASC',
                            'orderby' => 'title'
                     ) );
    return $qr;
}

//lay img
if (!function_exists('getPostImage')) {
    /**
     * [getPostImage description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    function getPostImage($id, $imageSize = '')
    {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id), $imageSize);
        return (!$img) ? asset('images/no-image.png') : $img[0];
    }
}

//getCategories
function getCategories($tax) {
    global $wpdb;

    $sql = "SELECT * FROM {$wpdb->prefix}terms as terms
            JOIN {$wpdb->prefix}term_taxonomy as term_tax ON terms.term_id = term_tax.term_id
            WHERE term_tax.taxonomy = '{$tax}'";

    $results = $wpdb->get_results($sql);

    $cates = [];

    foreach ($results as $term) {
        $cates[$term->term_id] = $term->name;
    }

    return $cates;
}

//cat chuoi
function cut_string($str,$len,$more){
	if ($str=="" || $str==NULL) return $str;
	if (is_array($str)) return $str;
		$str = trim(strip_tags($str));
	if (strlen($str) <= $len) return $str;
		$str = substr($str,0,$len);
	if ($str != "") {
		if (!substr_count($str," ")) {
		  if ($more) $str .= " ...";
		  return $str;
		}
		while(strlen($str) && ($str[strlen($str)-1] != " ")) {
			$str = substr($str,0,-1);
		}
		$str = substr($str,0,-1);
		if ($more) $str .= " ...";
	}
	return $str;
}

//lay duong dan
if (!function_exists('asset')) {
    function asset($path)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/dist/' . $path);
    }
}

//truyền title lên head trang khi ko dùng yoast seo
if (!function_exists('title')) {
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_bloginfo('name');
        }

        if (is_archive()) {
            $obj = get_queried_object();
            return $obj->name;
        }

        if (is_404()) {
            return '404 page not found';
        }

        return get_the_title();
    }
}

//shortcode có thể chạy được trong widget
add_filter('widget_text','do_shortcode');

//count views post
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

//Bộ lọc search
if( !is_admin() ) {
    function filter_search($query) {
        if ($query->is_search) {
            $query->set('post_type', array('san-pham', 'post'));
        };
        return $query;
    };
    add_filter('pre_get_posts', 'filter_search');
}

//tao bo loc san pham nhu tin tuc
function tsm_filter_post_type_by_taxonomy() {
    global $typenow;
    $post_type = 'diem-ban';
    $taxonomy  = 'diem-ban-category';
    if ($typenow == $post_type) {
        $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
        $info_taxonomy = get_taxonomy($taxonomy);
        wp_dropdown_categories(array(
            'show_option_all' => __("Show All {$info_taxonomy->label}"),
            'taxonomy'        => $taxonomy,
            'name'            => $taxonomy,
            'orderby'         => 'name',
            'selected'        => $selected,
            'show_count'      => true,
            'hide_empty'      => false,
            'hierarchical'    => 1,
        ));
    };
}
add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');

function tsm_convert_id_to_term_in_query($query) {
    global $pagenow;
    $post_type = 'diem-ban';
    $taxonomy  = 'diem-ban-category';
    $q_vars    = &$query->query_vars;
    if ( $pagenow == 'edit.php'   &&   isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type   &&   isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
        $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
        $q_vars[$taxonomy] = $term->slug;
    }
}
add_filter('parse_query', 'tsm_convert_id_to_term_in_query');

//Xóa footer trong admin
function remove_footer_admin () {
    echo 'Thiết kế website bởi <a href="https://www.facebook.com/tj3ungunhj9" target="_blank">Tiệp</a>';
}
add_filter('admin_footer_text', 'remove_footer_admin');
