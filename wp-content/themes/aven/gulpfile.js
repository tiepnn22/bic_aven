var gulp = require('gulp');         //gọi gulp
var sass = require('gulp-sass');    //chuyển sass thành css
var cssmin = require('gulp-cssmin');//nén min file
var jsmin = require('gulp-jsmin');//nén min file
var concat = require("gulp-concat");//nối nội dung file
var bust = require('gulp-buster');  //tạo bộ nhớ cache
var imagemin = require('gulp-imagemin');//nén ảnh
var cache = require('gulp-cache');      //tạo cache lưu ảnh trên local
var del = require('del');               //xoá
var runSequence = require('run-sequence');//đảm bảo các task chạy tuần tự trong default gulp
var watch = require('watch');//theo dõi sự thay đổi của file va tu dong gulp

//gulp css
gulp.task('sass', function(){
    return gulp.src([
        'assets/css/index.scss',
        // 'assets/css/scss/**/*.scss',
    ])
    .pipe(sass())
    .pipe(concat('style.css'))
    .pipe(gulp.dest('dist/css/'))
});
//gulp thu vien css va css
gulp.task('css', function(){
    return gulp.src([
        'bower_components/font-awesome/css/font-awesome.min.css',
        'bower_components/bootstrap/dist/css/bootstrap.min.css',
        'bower_components/jquery.meanmenu/meanmenu.min.css',
        'dist/css/style.css',
    ])
    .pipe(cssmin())
    .pipe(concat('style.all.css'))
    .pipe(gulp.dest('dist/css/'))
    .pipe(bust({relativePath: './dist/css'}))
    .pipe(gulp.dest('dist/.cache/'))
});
//gulp thu vien js va js
gulp.task('js', function(){
    return gulp.src([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/jquery.meanmenu/jquery.meanmenu.min.js',
        'assets/js/main.js',
    ])
    // .pipe(jsmin()) //min lau lam nen lam khi ban giao khach thoi
    .pipe(concat('main.all.js'))
    .pipe(gulp.dest('dist/js/'))
    .pipe(bust({relativePath: './dist/js'}))
    .pipe(gulp.dest('dist/.cache/'))
});
//nén tất cả dạng ảnh
gulp.task('images', function(){
    return gulp.src('assets/images/**/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(cache(imagemin({
        interlaced: true
    })))
    .pipe(gulp.dest('dist/images'))
});
//coppy fonts
gulp.task('fonts', function() {
    return gulp.src([
        'bower_components/font-awesome/fonts/**/*',
        'assets/fonts/**/*',
    ])
    .pipe(gulp.dest('dist/fonts'))
})
//xoá dist mỗi lần gulp
gulp.task('clean', function() {
    return del.sync('dist');
})
//lệnh gulp default
gulp.task('default', function () {
    runSequence('clean', 'sass', 'css', 'js', 'images', 'fonts')
})
//watch
gulp.task('watch', function(){
    gulp.watch('assets/**/*', ['default']); 
})

//gulp-browserify//kiem tra css
//gulp-rename    //đổi tên file
//gulp-sass-glob //chọn nhiều file
//gulp-jshint    //kiểm tra code js
//gulp-uglify    //giảm thiểu javascript

